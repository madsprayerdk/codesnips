﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using GameServerQuery.ServerType;
using ManoSoftware.Extensions.ArrayExtensions;

namespace GameServerQuery
{
    public class GameServerQuery<T> where T : IServer
    {
        public T Info { private set; get; }

        public string Ip { set; get; }
        public int Port { set; get; }
        public GameServerQuery(string ip, int port)
        {
            Ip = ip;
            Port = port;

            GetInfo();
        }

        public void GetInfo()
        {
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 4096);

            var queryEndPoint = new IPEndPoint(IPAddress.Parse(Ip), Port);
            var responseEndPoint = (EndPoint)queryEndPoint;

            Info = Activator.CreateInstance<T>();
            var buffer = new byte[100 * 1024];
            socket.SendTo(Info.RequestMessage(), queryEndPoint);
            var read = socket.ReceiveFrom(buffer, ref responseEndPoint);
            
            Info.ExtractInformation(buffer.SubArray(0, read));
        }
    }
}
