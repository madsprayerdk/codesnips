﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManoSoftware.Extensions.ArrayExtensions;
using static System.Text.Encoding;

namespace GameServerQuery.ServerType
{
    public class Source : IServer
    {
        // Information Fields
        public int ProtocolVersion { private set; get; }
        public string ServerName { private set; get; }
        public string MapName { private set; get; }
        public string GameFolderName { private set; get; }
        public string GameName { private set; get; }
        public int ApplicationId { private set; get; }
        public int CurrentPlayers { private set; get; }
        public int MaxPlayers { private set; get; }
        public int CurrentBots { private set; get; }
        public Enum.ServerType ServerType { private set; get;}
        public Enum.ServerPlatform ServerPlatform { private set; get; }
        public bool PublicVisible { private set; get; }
        public bool Secured { private set; get; }
        public string GameVersion { private set; get; }
        public int ServerPort { private set; get; }
        public Int64 ServerSteamId { private set; get; }
        public string Keyworkds { private set; get; }
        public Int64 LongGameID { private set; get; }

        public byte[] RequestMessage()
        {
            return new byte[]
            {
                0xFF, 0xFF, 0xFF, 0xFF, 0x54, 0x53, 0x6F, 0x75, 0x72, 0x63, 0x65, 0x20, 0x45, 0x6E, 0x67, 0x69, 0x6E,
                0x65, 0x20, 0x51, 0x75, 0x65, 0x72, 0x79, 0x00
            };
        }

        public void ExtractInformation(byte[] response)
        {
            int cursor = 0;

            // Skip first padding bytes and skip header
            while (response[cursor] != 0x49)
                cursor++;
            cursor++;

            // Extract protocol version number
            ProtocolVersion = response[cursor];
            cursor++;

            // Get Server Name
            var nameStartPosition = cursor;

            while (response[cursor] != 0x00)
                cursor++;

            var nameBytes = response.SubArray(nameStartPosition, cursor - nameStartPosition);
            ServerName = UTF8.GetString(nameBytes);
            cursor++;

            // Get current map
            var mapNameStart = cursor;

            while (response[cursor] != 0x00)
                cursor++;

            var mapNameBytes = response.SubArray(mapNameStart, cursor - mapNameStart);
            MapName = UTF8.GetString(mapNameBytes);
            cursor++;

            //Get folder name
            var folderNameStart = cursor;

            while (response[cursor] != 0x00)
                cursor++;

            var folderNameBytes = response.SubArray(folderNameStart, cursor - folderNameStart);
            GameFolderName = UTF8.GetString(folderNameBytes);
            cursor++;

            // Get Game Name
            var gameNameStart = cursor;

            while (response[cursor] != 0x00)
                cursor++;

            var gameNameBytes = response.SubArray(gameNameStart, cursor - gameNameStart);
            GameName = UTF8.GetString(gameNameBytes);
            cursor++;

            // Get Application ID
            var versionBytes = response.SubArray(cursor, 2);
            ApplicationId = BitConverter.ToInt16(versionBytes, 0);
            cursor += 2;

            // Current Players
            CurrentPlayers = response[cursor];
            cursor++;

            // Max Players
            MaxPlayers = response[cursor];
            cursor++;

            // Current Bots
            CurrentBots = response[cursor];
            cursor++;


        }
    } 
}
